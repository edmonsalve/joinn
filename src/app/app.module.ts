import { QRScanner } from '@ionic-native/qr-scanner';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RecoveryPasswordPage } from "../pages/recovery-password/recovery-password";
import { EventDetailPage } from './../pages/home/event-detail/event-detail';
import { ProfilePage } from './../pages/profile/profile';
import { ServicesPage } from './../pages/services/services';
import { ShoppingPage } from './../pages/shopping/shopping';
import { LoginPage } from './../pages/login/login';
import { AddedServicesPage } from './../pages/added-services/added-services';
import { ServiceDetailPage } from './../pages/service-detail/service-detail';
import { WriteUsPage } from './../pages/write-us/write-us';
import { MyShoppingPage } from './../pages/my-shopping/my-shopping';
import { UpdateProfilePage } from './../pages/update-profile/update-profile';
import { ReferedFriendPage } from './../pages/refered-friend/refered-friend';
import { MyEventsPage } from './../pages/my-events/my-events';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RecoveryPasswordPage,
    EventDetailPage,
    ProfilePage,
    ServicesPage,
    ShoppingPage,
    AddedServicesPage,
    ServiceDetailPage,
    WriteUsPage,
    UpdateProfilePage,
    MyShoppingPage,
    ReferedFriendPage,
    MyEventsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RecoveryPasswordPage,
    EventDetailPage,
    ProfilePage,
    ServicesPage,
    ShoppingPage,
    AddedServicesPage,
    ServiceDetailPage,
    WriteUsPage,
    UpdateProfilePage,
    MyShoppingPage,
    ReferedFriendPage,
    MyEventsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    QRScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
