import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';

import { ServicesPage } from './../services/services';
import { ProfilePage } from './../profile/profile';
import { EventDetailPage } from './event-detail/event-detail';
import { ShoppingPage } from '../shopping/shopping';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  public navigateBySwip(event) {
    if (event.direction == 4) {
      // Navegar a perfil
      this.navCtrl.push(ProfilePage);
    } else {
      // Navegar a servicios
      this.navCtrl.push(ServicesPage);
    }
  }

  public goToEventDetail() {
    this.navCtrl.push(EventDetailPage);
  }

  public goToShop() {
    this.navCtrl.push(ShoppingPage);
  }

}
