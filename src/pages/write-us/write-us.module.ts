import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WriteUsPage } from './write-us';

@NgModule({
  declarations: [
    WriteUsPage,
  ],
  imports: [
    IonicPageModule.forChild(WriteUsPage),
  ],
})
export class WriteUsPageModule {}
