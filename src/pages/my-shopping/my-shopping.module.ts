import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyShoppingPage } from './my-shopping';

@NgModule({
  declarations: [
    MyShoppingPage,
  ],
  imports: [
    IonicPageModule.forChild(MyShoppingPage),
  ],
})
export class MyShoppingPageModule {}
