import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReferedFriendPage } from './refered-friend';

@NgModule({
  declarations: [
    ReferedFriendPage,
  ],
  imports: [
    IonicPageModule.forChild(ReferedFriendPage),
  ],
})
export class ReferedFriendPageModule {}
