import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RecoveryPasswordPage } from './../recovery-password/recovery-password';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public password: string = "";
  public username: string = "";
  public loading: any;

  constructor (
    public navCtrl: NavController, 
    public navParams: NavParams,
    private loadingCtrl: LoadingController
  ) {
  }

  ionViewDidLoad() {
    this.loading = this.loadingCtrl.create({
      content: "Espere un momento"
    });
  }

  public login () {
    if (this.username != "" && this.password != "") {
      this.loading.present();
      setTimeout(() => {
        this.loading.dismiss();
        this.navCtrl.push(HomePage);
      }, 3000);
    }
  }

  // Navegar a la vista para recuperar el password
  public goToRecoveryPassword() {
    this.navCtrl.push(RecoveryPasswordPage);
  }

}
