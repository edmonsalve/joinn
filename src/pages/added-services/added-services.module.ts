import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddedServicesPage } from './added-services';

@NgModule({
  declarations: [
    AddedServicesPage,
  ],
  imports: [
    IonicPageModule.forChild(AddedServicesPage),
  ],
})
export class AddedServicesPageModule {}
