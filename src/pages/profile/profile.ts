import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { UpdateProfilePage } from './../update-profile/update-profile';
import { MyShoppingPage } from './../my-shopping/my-shopping';
import { WriteUsPage } from './../write-us/write-us';
import { ReferedFriendPage } from './../refered-friend/refered-friend';
import { MyEventsPage } from './../my-events/my-events';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  public goToUpdateProfile() {
    this.navCtrl.push(UpdateProfilePage);
  }

  public goToMyShoppings() {
    this.navCtrl.push(MyShoppingPage);
  }

  public goToWriteUs() {
    this.navCtrl.push(WriteUsPage);
  }

  public goToReferedFriend() {
    this.navCtrl.push(ReferedFriendPage);
  }

  public goToMyEvents() {
    this.navCtrl.push(MyEventsPage);
  }

}
