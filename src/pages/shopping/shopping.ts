import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';


@IonicPage()
@Component({
  selector: 'page-shopping',
  templateUrl: 'shopping.html',
})
export class ShoppingPage {

  constructor (
    public navCtrl: NavController, 
    public navParams: NavParams,
    private qrScanner: QRScanner
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShoppingPage');
  }

  public openScanner() {
    console.log("clicked");
    this.qrScanner.prepare()
        .then((status: QRScannerStatus) => {
          if (status.authorized) {
            let scanSub = this.qrScanner.scan().subscribe((text: string) => {
              console.log('Scanned something', text);
     
              this.qrScanner.hide(); // hide camera preview
              scanSub.unsubscribe(); // stop scanning
            });
            // show camera preview
            this.qrScanner.show();
       
              // wait for user to scan something, then the observable callback will be called
       
          } else if (status.denied) {
            console.log("Permisos permanentemente denegados, de camara")
              // camera permission was permanently denied
              // you must use QRScanner.openSettings() method to guide the user to the settings page
              // then they can grant the permission from there
          } else {
            console.log("Permiso denegado termporalmente");
              // permission was denied, but not permanently. You can ask for permission again at a later time.
          }
        }).catch((error: any) => {
          console.log("Error is: "+ error);
        })
  }

  public close() {
    this.navCtrl.pop();
  }

}
